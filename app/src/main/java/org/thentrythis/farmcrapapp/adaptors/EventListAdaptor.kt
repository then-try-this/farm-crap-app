package org.thentrythis.farmcrapapp.adaptors
/*
   Sward App Copyright (C) 2020 FoAM Kernow

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import org.thentrythis.farmcrapapp.EventActivity
import org.thentrythis.farmcrapapp.R
import org.thentrythis.farmcrapapp.data.CrapAppDateUtil
import org.thentrythis.farmcrapapp.data.Event

class EventListAdapter internal constructor (
    private var context: Context,
    private var fragment: Fragment? = null
) : RecyclerView.Adapter<EventListAdapter.EventViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var events = emptyList<Event>() // Cached copy of fields

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val eventItemView: Button = itemView.findViewById(R.id.eventButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val itemView = inflater.inflate(R.layout.event_item, parent, false)
        return EventViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val current = events[position]
        val time = CrapAppDateUtil.convertDateToDisplay(current.time)
        holder.eventItemView.text = context.getString(R.string.event_list_button,time,current.field.fieldName)
        holder.eventItemView.setOnClickListener {
            if (fragment==null) {
                Intent(context, EventActivity::class.java).let {
                    it.putExtra("EVENT_ID", current.eventId)
                    context.startActivity(it)
                }
            } else {
                //val bundle = bundleOf("field_id" to current.fieldId)
                //fragment!!.findNavController().navigate(R.id.action_surveyFieldFragment_to_surveyHowtoFragment,bundle)
            }

        }
    }

    internal fun setEvents(events: List<Event>) {
        this.events = events
        notifyDataSetChanged()
    }

    override fun getItemCount() = events.size
}