package org.thentrythis.farmcrapapp.adaptors

/*
   Sward App Copyright (C) 2020 FoAM Kernow

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import org.thentrythis.farmcrapapp.ManureActivity
import org.thentrythis.farmcrapapp.R
import org.thentrythis.farmcrapapp.data.Manure

class ManureListAdapter internal constructor (
    private var context: Context,
    private var fragment: Fragment? = null
) : RecyclerView.Adapter<ManureListAdapter.ManureViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var manures = emptyList<Manure>() // Cached copy of fields

    inner class ManureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val manureItemView: Button = itemView.findViewById(R.id.manureButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManureViewHolder {
        val itemView = inflater.inflate(R.layout.manure_item, parent, false)
        return ManureViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ManureViewHolder, position: Int) {
        val current = manures[position]
        holder.manureItemView.text = current.manureName
        holder.manureItemView.setOnClickListener {
            if (fragment==null) {
                Intent(context, ManureActivity::class.java).let {
                    it.putExtra("MANURE_ID", current.manureId)
                    context.startActivity(it)
                }
            } else {
                //val bundle = bundleOf("field_id" to current.fieldId)
                //fragment!!.findNavController().navigate(R.id.action_surveyFieldFragment_to_surveyHowtoFragment,bundle)
            }

        }
    }

    internal fun setManures(manures: List<Manure>) {
        this.manures = manures
        notifyDataSetChanged()
    }

    override fun getItemCount() = manures.size
}