package org.thentrythis.farmcrapapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import org.thentrythis.farmcrapapp.adaptors.FieldListAdapter
import org.thentrythis.farmcrapapp.adaptors.ManureListAdapter
import org.thentrythis.farmcrapapp.data.AppSetting
import org.thentrythis.farmcrapapp.databinding.ActivitySetupBinding

class SetupActivity : BaseActivity() {

    private lateinit var binding: ActivitySetupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySetupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu)
        setSupportActionBar(binding.toolbar)

        val adapter = FieldListAdapter(this)
        binding.fieldsRecycler.adapter = adapter
        binding.fieldsRecycler.layoutManager = LinearLayoutManager(this)

        crapAppViewModel.allFields.observe(this) { fields ->
            // Update the cached copy of the words in the adapter.
            fields?.let { adapter.setFields(it) }
        }

        binding.newFieldButton.setOnClickListener {
            val intent = Intent(this@SetupActivity, FieldActivity::class.java)
            intent.putExtra("FIELD_ID", 0L) // prob not needed as it's the default
            startActivity(intent)
        }
        
        val manureAdapter = ManureListAdapter(this)
        binding.manureRecycler.adapter = manureAdapter
        binding.manureRecycler.layoutManager = LinearLayoutManager(this)

        crapAppViewModel.allManures.observe(this) { manures ->
            // Update the cached copy of the words in the manureAdapter.
            manures?.let { manureAdapter.setManures(it) }
        }

        binding.newManureButton.setOnClickListener {
            val intent = Intent(this@SetupActivity, ManureActivity::class.java)
            intent.putExtra("MANURE_ID", 0L) // prob not needed as it's the default
            startActivity(intent)
        }

        fun Float.format(digits: Int) = "%.${digits}f".format(this)

        // seems to be the only way!
        var cost_n_set = false
        crapAppViewModel.getSetting("cost_n").observe(this) {
            it?.let {
                if (!cost_n_set) {
                    Log.i("crapapp",it.value.toString())
                    binding.costNEditText.setText(it.value.format(2))
                    cost_n_set=true
                }
            }
        }

        var cost_p_set = false
        crapAppViewModel.getSetting("cost_p").observe(this) {
            it?.let {
                if (!cost_p_set) {
                    binding.costPEditText.setText(it.value.format(2))
                    cost_p_set=true
                }
            }
        }

        var cost_k_set = false
        crapAppViewModel.getSetting("cost_k").observe(this) {
            it?.let {
                if (!cost_k_set) {
                    binding.costKEditText.setText(it.value.format(2))
                    cost_k_set=true
                }
            }
        }

        var cost_mg_set = false
        crapAppViewModel.getSetting("cost_mg").observe(this) {
            it?.let {
                if (!cost_mg_set) {
                    binding.costMgEditText.setText(it.value.format(2))
                    cost_mg_set=true
                }
            }
        }

        var cost_s_set = false
        crapAppViewModel.getSetting("cost_s").observe(this) {
            it?.let {
                if (!cost_s_set) {
                    binding.costSEditText.setText(it.value.format(2))
                    cost_s_set = true
                }
            }
        }

        crapAppViewModel.getSetting("currency").observe(this) {
            it?.let {
                if (binding.currencySwitch.isActivated && it.value==0F) {
                    binding.currencySwitch.isChecked=false
                }
                if (!binding.currencySwitch.isActivated && it.value==1F) {
                    binding.currencySwitch.isChecked=true
                }
            }
        }

        binding.costNEditText.addTextChangedListener(MoneyTextWatcher(binding.costNEditText))
        binding.costPEditText.addTextChangedListener(MoneyTextWatcher(binding.costPEditText))
        binding.costKEditText.addTextChangedListener(MoneyTextWatcher(binding.costKEditText))
        binding.costMgEditText.addTextChangedListener(MoneyTextWatcher(binding.costMgEditText))
        binding.costSEditText.addTextChangedListener(MoneyTextWatcher(binding.costSEditText))

        binding.currencySwitch.setOnCheckedChangeListener { _, isChecked ->
            crapAppViewModel.updateSetting(
                AppSetting(
                    "currency", if (isChecked) {
                        1f
                    } else {
                        0f
                    }
                )
            )
        }
        binding.calculatorButton.setOnClickListener {
            tryToStartCalculator(Intent(this, CalculatorActivity::class.java))
        }
    }

    fun floatFromCurrencyStr(string: String): Float {
        if (string.length>1) {
            return string.replace("[$,£]".toRegex(), "").toFloat()
        }
        return 0.0F
    }

    // every way out (except 'back') calls onPause
    override fun onPause() {
        super.onPause()
        crapAppViewModel.updateSetting(AppSetting("cost_n",floatFromCurrencyStr(binding.costNEditText.text.toString())))
        crapAppViewModel.updateSetting(AppSetting("cost_p",floatFromCurrencyStr(binding.costPEditText.text.toString())))
        crapAppViewModel.updateSetting(AppSetting("cost_k",floatFromCurrencyStr(binding.costKEditText.text.toString())))
        crapAppViewModel.updateSetting(AppSetting("cost_mg",floatFromCurrencyStr(binding.costMgEditText.text.toString())))
        crapAppViewModel.updateSetting(AppSetting("cost_s",floatFromCurrencyStr(binding.costSEditText.text.toString())))
    }
}

