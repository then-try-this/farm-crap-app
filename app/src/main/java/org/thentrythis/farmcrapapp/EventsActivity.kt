package org.thentrythis.farmcrapapp

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import org.thentrythis.farmcrapapp.adaptors.EventListAdapter
import org.thentrythis.farmcrapapp.databinding.ActivityEventsBinding

class EventsActivity : BaseActivity() {

    private lateinit var binding: ActivityEventsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityEventsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu)
        setSupportActionBar(binding.toolbar)

        val adapter = EventListAdapter(this)
        binding.eventsRecycler.adapter = adapter
        binding.eventsRecycler.layoutManager = LinearLayoutManager(this)

        crapAppViewModel.allEvents.observe(this) { fields ->
            // Update the cached copy of the words in the adapter.
            fields?.let { adapter.setEvents(it) }
        }

    }
}