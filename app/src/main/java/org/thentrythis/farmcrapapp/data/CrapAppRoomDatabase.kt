package org.thentrythis.farmcrapapp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities= [Field::class, Manure::class, Event::class, AppSetting::class], version = 5, exportSchema = false)
abstract class CrapAppRoomDatabase : RoomDatabase() {

    abstract fun crapAppDao(): CrapAppDao

    private class SwardDatabaseCallback(
        private val scope: CoroutineScope
    ) : Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    val crapAppDao = database.crapAppDao()
                    // Add sample fields
                    //val fieldId = crapAppDao.insertField(Field("Top field", 20.0F))
                    //val manureId = crapAppDao.insertManure(Manure("Poo", 20.0F, 2F, 3F, 5F ,4F, "" ))
                }
            }
        }
    }

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: CrapAppRoomDatabase? = null

        /*val MIGRATION_4_5 = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // add complete int to surveys, and default them to true
                database.execSQL("""
                    ALTER TABLE survey_table ADD complete INTEGER
                    """.trimIndent())
            }
        }*/

        fun getDatabase(context: Context, scope: CoroutineScope): CrapAppRoomDatabase {

            //context.deleteDatabase("sward_database")

            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CrapAppRoomDatabase::class.java,
                    "sward_database")
                    //.addMigrations(MIGRATION_4_5)
                    .fallbackToDestructiveMigration() // TODO: remove
                    .addCallback(SwardDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

