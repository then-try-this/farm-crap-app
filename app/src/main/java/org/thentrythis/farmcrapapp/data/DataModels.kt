/*
   Farm Crap App (C) 2023 Then Try This

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.thentrythis.farmcrapapp.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="app_setting")
data class AppSetting(@PrimaryKey(autoGenerate = false)
    var name:String = "",var value: Float)

@Entity(tableName="manure_table")
data class Manure(var manureName: String,
                 var n_content: Float,
    var p_content: Float,
    var k_content: Float,
    var mg_content: Float,
    var s_content: Float,
    var quality: String) {
    @PrimaryKey(autoGenerate = true)
    var manureId:Long = 0
}

@Entity(tableName="field_table")
data class Field(var fieldName: String,
                 var size: Float) {
    @PrimaryKey(autoGenerate = true)
    var fieldId:Long = 0
}

@Entity(tableName="event_table")
data class Event(var time: String,
                 var amount: Float,
                 // record values here
                 // but we can also use fieldId/manureId stored to link
                 // back to the original field/manure
                 // these may have changed since the event was created
                 // which is why we store the calculated values above
                 @Embedded var manure: Manure,
                 @Embedded var field: Field,
                 var n_saving: Float,
                 var p_saving: Float,
                 var k_saving: Float,
                 var mg_saving: Float,
                 var s_saving: Float) {
    @PrimaryKey(autoGenerate = true)
    var eventId:Long = 0
}

