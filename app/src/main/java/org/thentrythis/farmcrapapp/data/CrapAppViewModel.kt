package org.thentrythis.farmcrapapp.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CrapAppViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CrapAppRepository

    // Reasons for the ViewModel
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.

    val allFields: LiveData<List<Field>>
    val allManures: LiveData<List<Manure>>
    val allEvents: LiveData<List<Event>>

    fun getField(fieldId: Long) : LiveData<Field> = repository.getField(fieldId)
    fun getFieldByName(fieldName: String) : LiveData<Field> = repository.getFieldByName(fieldName)
    fun getManure(manureId: Long) : LiveData<Manure> = repository.getManure(manureId)
    fun getManureByName(manureName: String) : LiveData<Manure> = repository.getManureByName(manureName)
    fun getEvent(eventId: Long) : LiveData<Event> = repository.getEvent(eventId)
    fun getSetting(name: String) : LiveData<AppSetting> = repository.getSetting(name)

    init {
        val crapAppDao = CrapAppRoomDatabase.getDatabase(application,viewModelScope).crapAppDao()
        repository = CrapAppRepository(crapAppDao)
        allFields = repository.allFields
        allManures = repository.allManures
        allEvents = repository.allEvents
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */

    fun insertField(field: Field) = viewModelScope.launch(Dispatchers.IO) {
        val fieldId = repository.insertField(field)
    }

    fun updateField(field: Field) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateField(field)
    }

    fun updateManure(manure: Manure) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateManure(manure)
    }

    fun updateSetting(setting: AppSetting) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateSetting(setting)
    }

    /*   fun surveyComplete(surveyId: Long) = viewModelScope.launch(Dispatchers.IO) {
        val survey = repository.syncGetSurvey(surveyId)
        survey[0]?.let {
            it.complete=1
            repository.updateSurvey(it)
        }
    }
*/
    fun insertManure(manure: Manure): LiveData<Long> {
        val liveData = MutableLiveData<Long>()
        viewModelScope.launch(Dispatchers.IO) {
            liveData.postValue(repository.insertManure(manure))
        }
        return liveData
    }

    fun insertEvent(event: Event) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertEvent(event)
    }

    fun deleteField(fieldId: Long) = viewModelScope.launch(Dispatchers.IO) {
        // delete all events for this field
        //repository.syncGetEvents(fieldId).forEach {
        //    repository.deleteEvent(it.eventId)
        //}
        repository.deleteField(fieldId)
    }

    fun deleteEvent(eventId: Long) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteEvent(eventId)
    }

    fun deleteManure(manureId: Long) = viewModelScope.launch(Dispatchers.IO) {
        // TODO: delete events referencing this manure???
        repository.deleteManure(manureId)
    }

    /*
    fun getExportData(): LiveData<List<FieldWithSurveysAndRecords>> {
        val liveData = MutableLiveData<List<FieldWithSurveysAndRecords>>()
        viewModelScope.launch(Dispatchers.IO) {
            liveData.postValue(repository.syncGetExportData())
        }
        return liveData
    }
     */

}