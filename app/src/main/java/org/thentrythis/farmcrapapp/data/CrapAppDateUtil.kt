package org.thentrythis.farmcrapapp.data

import java.text.SimpleDateFormat
import java.util.Locale

class CrapAppDateUtil {
    companion object {
        fun convertDateToDisplay(datestring: String): String {
            // convert stored/sortable date string to a more readable form
            val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            fromFormat.isLenient=true
            val fromDate = fromFormat.parse(datestring)
            val showFormat = SimpleDateFormat("d MMM yyyy", Locale.getDefault());
            fromDate?.let {
                return showFormat.format(fromDate)
            }
            return "?"
        }
    }
}
