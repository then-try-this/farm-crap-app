package org.thentrythis.farmcrapapp.data

import androidx.lifecycle.LiveData

class CrapAppRepository(private val crapAppDao: CrapAppDao) {

    val allFields: LiveData<List<Field>> = crapAppDao.getFields()
    val allManures: LiveData<List<Manure>> = crapAppDao.getManures()
    val allEvents: LiveData<List<Event>> = crapAppDao.getEvents()

    fun getField(fieldId: Long): LiveData<Field> = crapAppDao.getField(fieldId)
    fun getFieldByName(fieldName: String): LiveData<Field> = crapAppDao.getFieldByName(fieldName)
    fun getManure(manureId: Long): LiveData<Manure> = crapAppDao.getManure(manureId)
    fun getManureByName(manureName: String): LiveData<Manure> = crapAppDao.getManureByName(manureName)
    fun getEvent(eventId: Long): LiveData<Event> = crapAppDao.getEvent(eventId)
    fun getSetting(name: String): LiveData<AppSetting> = crapAppDao.getSetting(name)

    suspend fun insertField(field: Field): Long = crapAppDao.insertField(field)
    suspend fun updateField(field: Field) = crapAppDao.updateField(field)
    suspend fun insertManure(manure: Manure): Long = crapAppDao.insertManure(manure)
    suspend fun updateManure(manure: Manure) = crapAppDao.updateManure(manure)
    suspend fun insertEvent(event: Event) = crapAppDao.insertEvent(event)
    suspend fun updateSetting(setting: AppSetting) = crapAppDao.insertSetting(setting)

    suspend fun deleteAllFields() = crapAppDao.deleteAllFields()
    suspend fun deleteAllManures() = crapAppDao.deleteAllManures()
    suspend fun deleteAllEvents() = crapAppDao.deleteAllEvents()
    suspend fun deleteField(fieldId: Long) = crapAppDao.deleteField(fieldId)
    suspend fun deleteManure(manureId: Long) = crapAppDao.deleteManure(manureId)
    suspend fun deleteEvent(eventId: Long) = crapAppDao.deleteEvent(eventId)

    //suspend fun syncGetEvents(fieldId: Long) = crapAppDao.syncGetEvents(fieldId)

}