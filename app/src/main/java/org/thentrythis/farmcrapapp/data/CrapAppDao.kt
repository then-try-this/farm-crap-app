package org.thentrythis.farmcrapapp.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CrapAppDao {
    @Query("SELECT * from field_table ORDER BY fieldName ASC")
    fun getFields(): LiveData<List<Field>>

    @Query("SELECT * from field_table Where fieldId=:fieldId")
    fun getField(fieldId: Long): LiveData<Field>
    @Query("SELECT * from field_table Where fieldName=:fieldName")
    fun getFieldByName(fieldName: String): LiveData<Field>

    @Query("SELECT * from manure_table Where manureId=:manureId")
    fun getManure(manureId: Long): LiveData<Manure>
    @Query("SELECT * from manure_table Where manureName=:manureName")
    fun getManureByName(manureName: String): LiveData<Manure>
    @Query("SELECT * from manure_table ORDER BY manureName ASC")
    fun getManures(): LiveData<List<Manure>>


    @Query("SELECT * from event_table Where eventId=:eventId")
    fun getEvent(eventId: Long): LiveData<Event>
    @Query("SELECT * from event_table ORDER BY time ASC")
    fun getEvents(): LiveData<List<Event>>


    @Query("SELECT * from app_setting Where name=:settingName")
    fun getSetting(settingName: String): LiveData<AppSetting>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSetting(setting: AppSetting)

    //@Query("SELECT * from manure_table Where manureId=:manureId")
    //fun getManure(manureId: Long): LiveData<Field>

    //@Transaction
    //@Query("SELECT * from event_table Where fieldId=:fieldId")
    //fun getEvents(fieldId: Long): LiveData<List<Event>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertField(field: Field): Long
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertManure(survey: Manure): Long
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEvent(sown: Event): Long

    @Update
    suspend fun updateField(field: Field)
    @Update
    suspend fun updateManure(manure: Manure)

    @Query("DELETE FROM field_table")
    suspend fun deleteAllFields()
    @Query("DELETE FROM manure_table")
    suspend fun deleteAllManures()
    @Query("DELETE FROM event_table")
    suspend fun deleteAllEvents()
    @Query("DELETE FROM field_table where fieldId=:fieldId")
    suspend fun deleteField(fieldId: Long)
    @Query("DELETE FROM manure_table where manureId=:manureId")
    suspend fun deleteManure(manureId: Long)
    @Query("DELETE FROM event_table where eventId=:eventId")
    suspend fun deleteEvent(eventId: Long)

    // blocking versions
//    @Transaction
//    @Query("SELECT * from field_table")
//    fun syncGetExportData(): List<FieldWithSurveysAndRecords>

    //@Transaction
    //@Query("SELECT * from event_table where fieldId=:fieldId")
    //fun syncGetEvents(fieldId: Long): List<Event>

}
