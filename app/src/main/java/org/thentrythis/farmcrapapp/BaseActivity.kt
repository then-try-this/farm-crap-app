package org.thentrythis.farmcrapapp

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import org.thentrythis.farmcrapapp.data.CrapAppViewModel
import java.text.SimpleDateFormat
import java.util.Locale

open class BaseActivity : AppCompatActivity() {

    lateinit var crapAppViewModel: CrapAppViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        crapAppViewModel = ViewModelProvider(this)[CrapAppViewModel::class.java]
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    fun tryToStartCalculator(intent: Intent) {
        crapAppViewModel.allManures.observe(this) { manures ->
            manures?.let {
                if (manures.size > 0) {
                    crapAppViewModel.allFields.observe(this) { fields ->
                        fields?.let {
                            if (fields.size > 0) {
                                startActivity(intent)
                            } else {
                                Toast.makeText(this, R.string.calc_error_field, Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    }
                } else {
                    Toast.makeText(this, R.string.calc_error_manure, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button_normal, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {

            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                true }
            R.id.action_setup -> {
                val intent = Intent(this, SetupActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                true }
            R.id.action_calculator -> {
                val intent = Intent(this, CalculatorActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                tryToStartCalculator(intent)
                true }
            R.id.action_data -> {
                val intent = Intent(this, EventsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                true }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
