package org.thentrythis.farmcrapapp

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import org.thentrythis.farmcrapapp.data.Field
import org.thentrythis.farmcrapapp.databinding.ActivityFieldBinding

class FieldActivity : BaseActivity() {

    private lateinit var binding: ActivityFieldBinding
    private var fieldId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFieldBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu)
        setSupportActionBar(binding.toolbar)

        fieldId = intent.getLongExtra("FIELD_ID", 0)

        if (fieldId > 0) {
            crapAppViewModel.getField(fieldId).observe(this) { field ->
                field?.let {
                    binding.fieldNameEditText.setText(field.fieldName)
                    binding.fieldSizeEditText.setText(field.size.toString())
                }
            }
        }

        binding.fieldSaveButton.setOnClickListener {
            if (binding.fieldNameEditText.text.toString() != "" &&
                binding.fieldSizeEditText.text.toString() != "") {

                val field = Field(
                    binding.fieldNameEditText.text.toString(),
                    binding.fieldSizeEditText.text.toString().toFloat()
                )

                if (fieldId > 0) {
                    field.fieldId = fieldId
                    crapAppViewModel.updateField(field)
                } else {
                    crapAppViewModel.insertField(field)
                }
                setResult(Activity.RESULT_OK)
                finish()
            } else {
                if (binding.fieldNameEditText.text.toString() == "") {
                    Toast.makeText(this, R.string.calc_error_name, Toast.LENGTH_SHORT).show()
                }
                if (binding.fieldSizeEditText.text.toString() == "") {
                    Toast.makeText(this, R.string.calc_error_size, Toast.LENGTH_SHORT).show()
                }
            }
        }

        binding.fieldDeleteButton.setOnClickListener {
            if (fieldId>0) {
                crapAppViewModel.deleteField(fieldId)
            }
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}
