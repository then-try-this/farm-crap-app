package org.thentrythis.farmcrapapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import org.thentrythis.farmcrapapp.data.Event
import org.thentrythis.farmcrapapp.data.Field
import org.thentrythis.farmcrapapp.data.Manure
import org.thentrythis.farmcrapapp.databinding.ActivityCalculatorBinding
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class CalculatorActivity : BaseActivity() {

    private lateinit var binding: ActivityCalculatorBinding

    var event: Event=Event("",0F,
                        Manure("",0F,0F,0F,0F,0F,""),
                        Field("",0F),
        0F,0F,0F,0F,0F)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCalculatorBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu)
        setSupportActionBar(binding.toolbar)

        crapAppViewModel.getSetting("currency").observe(this) {
            it?.let{
                if (it.value==1F) { // arg
                    binding.calcTableSavings.text = getText(R.string.calc_table_savings_arg)
                } else { // us
                    binding.calcTableSavings.text = getText(R.string.calc_table_savings_us)
                }
            }
        }

        val fieldSpinnerAdapter = ArrayAdapter<Any>(this, android.R.layout.simple_spinner_item)
        crapAppViewModel.allFields.observe(this) { it ->
            it?.forEach {
                fieldSpinnerAdapter.add(it.fieldName)
            }
        }
        binding.fieldSpinner.adapter = fieldSpinnerAdapter
        binding.fieldSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                updateCalculator()
            }
        }

        val manureSpinnerAdapter = ArrayAdapter<Any>(this, android.R.layout.simple_spinner_item)
        crapAppViewModel.allManures.observe(this) { it ->
            it?.forEach {
                manureSpinnerAdapter.add(it.manureName)
            }
        }
        binding.manureSpinner.adapter = manureSpinnerAdapter
        binding.manureSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                updateCalculator()
            }
        }

        binding.amountEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                updateCalculator()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.saveButton.setOnClickListener {
            if (event.amount>0) {
                val calendar = Calendar.getInstance();
                calendar.set(
                    binding.date.year,
                    binding.date.month,
                    binding.date.dayOfMonth
                );
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                event.time = sdf.format(calendar.getTime());
                crapAppViewModel.insertEvent(event)
                Toast.makeText(this, R.string.calc_saved, Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, EventsActivity::class.java))
            } else {
                Toast.makeText(this, R.string.calc_error_amount, Toast.LENGTH_SHORT).show()
            }
        }

    }

    fun updateCalculator() {
        if (binding.amountEditText.text.toString()!="") {
            val amount = binding.amountEditText.text.toString().toFloat()
            val manureName = binding.manureSpinner.selectedItem.toString()
            val fieldName = binding.fieldSpinner.selectedItem.toString()

            // calculation
            crapAppViewModel.getFieldByName(fieldName).observe(this) { field ->
                crapAppViewModel.getManureByName(manureName).observe(this) { manure ->
                    val tonsPerHa = amount / field.size

                    event.amount = amount
                    event.field.fieldName = field.fieldName
                    event.field.fieldId = field.fieldId
                    event.manure.manureId = manure.manureId
                    event.manure.manureName = manure.manureName
                    event.manure.quality = manure.quality
                    // manure units are kg/ton - so this results in content kg per ha
                    event.manure.n_content = manure.n_content * tonsPerHa
                    event.manure.p_content = manure.p_content * tonsPerHa
                    event.manure.k_content = manure.k_content * tonsPerHa
                    event.manure.mg_content = manure.mg_content * tonsPerHa
                    event.manure.s_content = manure.s_content * tonsPerHa

                    fun Float.format(digits: Int) = "%.${digits}f".format(this)

                    binding.calcTableNAmount.text = event.manure.n_content.format(2)
                    binding.calcTablePAmount.text = event.manure.p_content.format(2)
                    binding.calcTableKAmount.text = event.manure.k_content.format(2)
                    binding.calcTableMgAmount.text = event.manure.mg_content.format(2)
                    binding.calcTableSAmount.text = event.manure.s_content.format(2)

                    crapAppViewModel.getSetting("cost_n").observe(this) {
                        it?.let {
                            // dollars per kg of nutrient * field size
                            event.n_saving = event.manure.n_content*it.value*field.size
                            binding.calcTableNSavings.text = event.n_saving.format(2)
                        }
                    }
                    crapAppViewModel.getSetting("cost_p").observe(this) {
                        it?.let {
                            event.p_saving = event.manure.p_content*it.value*field.size
                            binding.calcTablePSavings.text = event.p_saving.format(2)
                        }
                    }
                    crapAppViewModel.getSetting("cost_k").observe(this) {
                        it?.let {
                            event.k_saving = event.manure.k_content*it.value*field.size
                            binding.calcTableKSavings.text = event.k_saving.format(2)
                        }
                    }
                    crapAppViewModel.getSetting("cost_mg").observe(this) {
                        it?.let {
                            event.mg_saving = event.manure.mg_content*it.value*field.size
                            binding.calcTableMgSavings.text = event.mg_saving.format(2)
                        }
                    }
                    crapAppViewModel.getSetting("cost_s").observe(this) {
                        it?.let {
                            event.s_saving = event.manure.s_content*it.value*field.size
                            binding.calcTableSSavings.text = event.s_saving.format(2)
                        }
                    }
                }
            }
        }
    }
}
