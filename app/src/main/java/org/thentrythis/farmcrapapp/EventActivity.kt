package org.thentrythis.farmcrapapp

import android.app.Activity
import android.os.Bundle
import org.thentrythis.farmcrapapp.data.CrapAppDateUtil
import org.thentrythis.farmcrapapp.databinding.ActivityEventBinding
import java.text.SimpleDateFormat
import java.util.Locale

class EventActivity : BaseActivity() {

    private lateinit var binding: ActivityEventBinding
    private var eventId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityEventBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu)
        setSupportActionBar(binding.toolbar)

        crapAppViewModel.getSetting("currency").observe(this) {
            it?.let{
                if (it.value==1F) { // arg
                    binding.calcTableSavings.text = getText(R.string.calc_table_savings_arg)
                } else { // us
                    binding.calcTableSavings.text = getText(R.string.calc_table_savings_us)
                }
            }
        }

        eventId = intent.getLongExtra("EVENT_ID", 0)

        crapAppViewModel.getEvent(eventId).observe(this) {event ->
            event?.let {
                fun Float.format(digits: Int) = "%.${digits}f".format(this)
                binding.eventDate.text = CrapAppDateUtil.convertDateToDisplay(it.time)
                binding.eventField.text = it.field.fieldName
                binding.eventManure.text = it.manure.manureName
                binding.eventAmount.text = it.amount.format(2)
                binding.calcTableNAmount.text = it.manure.n_content.format(2)
                binding.calcTablePAmount.text = it.manure.p_content.format(2)
                binding.calcTableKAmount.text = it.manure.k_content.format(2)
                binding.calcTableMgAmount.text = it.manure.mg_content.format(2)
                binding.calcTableSAmount.text = it.manure.s_content.format(2)
                binding.calcTableNSavings.text = it.n_saving.format(2)
                binding.calcTablePSavings.text = it.p_saving.format(2)
                binding.calcTableKSavings.text = it.k_saving.format(2)
                binding.calcTableMgSavings.text = it.mg_saving.format(2)
                binding.calcTableSSavings.text = it.s_saving.format(2)
            }
        }

        binding.deleteEventButton.setOnClickListener {
            crapAppViewModel.deleteEvent(eventId)
            finish()
        }

    }
}
