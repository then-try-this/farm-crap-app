package org.thentrythis.farmcrapapp

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import org.thentrythis.farmcrapapp.data.Manure
import org.thentrythis.farmcrapapp.databinding.ActivityManureBinding

class ManureActivity : BaseActivity() {

    private lateinit var binding: ActivityManureBinding
    private var manureId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityManureBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu)
        setSupportActionBar(binding.toolbar)

        manureId = intent.getLongExtra("MANURE_ID", 0)

        if (manureId > 0) {
            Log.i("crapapp","reading: "+manureId)
            crapAppViewModel.getManure(manureId).observe(this) { manure ->
                manure?.let {
                    binding.manureNameEditText.setText(manure.manureName)
                    binding.manureNContent.setText(manure.n_content.toString())
                    binding.manurePContent.setText(manure.p_content.toString())
                    binding.manureKContent.setText(manure.k_content.toString())
                    binding.manureMgContent.setText(manure.mg_content.toString())
                    binding.manureSContent.setText(manure.s_content.toString())
                    val arr = resources.getStringArray(R.array.qualities)
                    binding.spinner.setSelection(arr.toList().indexOf(manure.quality))
                }
            }
        }

        binding.manureSaveButton.setOnClickListener {
            if (binding.manureNameEditText.text.toString()!="" &&
                binding.manureNContent.text.toString()!="" &&
                binding.manurePContent.text.toString()!="" &&
                binding.manureKContent.text.toString()!="" &&
                binding.manureMgContent.text.toString()!="" &&
                binding.manureSContent.text.toString()!=""
                ) {
                val manure = Manure(
                    binding.manureNameEditText.text.toString(),
                    binding.manureNContent.text.toString().toFloat(),
                    binding.manurePContent.text.toString().toFloat(),
                    binding.manureKContent.text.toString().toFloat(),
                    binding.manureMgContent.text.toString().toFloat(),
                    binding.manureSContent.text.toString().toFloat(),
                    binding.spinner.selectedItem.toString()
                )

                if (manureId > 0) {
                    manure.manureId = manureId
                    Log.i("crapapp","updating: "+manureId)
                    crapAppViewModel.updateManure(manure)
                } else {
                    Log.i("crapapp","creating: "+manureId)
                    crapAppViewModel.insertManure(manure)
                }
                finish()
            } else {
                if (binding.manureNameEditText.text.toString() == "") {
                    Toast.makeText(this, R.string.manure_error_name, Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, R.string.manure_error_content, Toast.LENGTH_SHORT).show()
                }

            }
        }

        binding.manureDeleteButton.setOnClickListener {
            if (manureId>0) {
                crapAppViewModel.deleteManure(manureId)
            }
            finish()
        }
    }
}
