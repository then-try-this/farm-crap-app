package org.thentrythis.farmcrapapp

import android.os.Bundle
import org.thentrythis.farmcrapapp.databinding.ActivityAboutBinding

class AboutActivity : BaseActivity() {

    private lateinit var binding: ActivityAboutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAboutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu)
        setSupportActionBar(binding.toolbar)

        binding.version.text =  getText(R.string.version_text).toString()+" "+packageManager.getPackageInfo(packageName,0).versionName
    }
}